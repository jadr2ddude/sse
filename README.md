# sse [![GoDoc](https://godoc.org/github.com/jadr2ddude/sse?status.svg)](https://godoc.org/github.com/jadr2ddude/sse) [![Build Status](https://travis-ci.org/jadr2ddude/sse.svg?branch=master)](https://travis-ci.org/jadr2ddude/sse) [![Coverage Status](https://coveralls.io/repos/github/jadr2ddude/sse/badge.svg?branch=master)](https://coveralls.io/github/jadr2ddude/sse?branch=master) [![Go Report Card](https://goreportcard.com/badge/github.com/jadr2ddude/sse)](https://goreportcard.com/report/github.com/jadr2ddude/sse)

Golang HTML5 Server-Sent-Events

According to the following specification: https://html.spec.whatwg.org/multipage/server-sent-events.html#server-sent-events (recommended by https://www.w3.org/TR/2015/REC-eventsource-20150203/)

### Example

See [example](https://github.com/jadr2ddude/sse/tree/master/example) for an example.
